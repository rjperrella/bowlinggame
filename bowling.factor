! Copyright (C) 2018 Ron Perrella.
! See http://factorcode.org/license.txt for BSD license.
!
! Based on Uncle Bob's implementation in F#
!
 
USING: kernel locals sequences math formatting combinators combinators.short-circuit prettyprint tools.test ;
IN: bowling

: isempty ( seq -- boolean )  length 0 = ;

: sum2of3 ( seq -- n ) first2 + ;

: strikeSum ( seq -- n ) 3 head [ second ] [ third ] bi + 10 + ; 

: sum3 ( seq -- n ) 3 head sum ;

: sum2 ( seq -- n ) 2 head sum ;

:: loopFrames ( rolls scores  -- seq )    
    { 
        { [ rolls isempty ] [ scores ] }
          
        { [ { [ rolls length 3 >= ] [ rolls first 10 = ] } 0&& ] 
          [ rolls 1 tail 
            scores rolls strikeSum suffix loopFrames ] }
              
        { [ { [ rolls length 3 >= ] [ rolls sum2of3 10 = ] } 0&& ] 
          [ rolls 2 tail 
            scores rolls sum3 suffix loopFrames ] }
              
        { [ rolls length 2 >= ] 
          [ rolls 2 tail 
            scores rolls sum2 suffix loopFrames ] }
        
    } cond ; 

: scoreFrames ( seq -- seq )  { } loopFrames ;

: score ( seq -- n ) scoreFrames 10 0 pad-tail 10 head sum ;

{ 0 }  [ { }        20 0 pad-tail score ] unit-test ! gutter game
{ 1 }  [ { 1 }      20 0 pad-tail score ] unit-test ! score of 1
{ 16 } [ { 5 5 3 }  20 0 pad-tail score ] unit-test ! spare
{ 24 } [ { 10 3 4 } 19 0 pad-tail score ] unit-test ! strike
{ 300 } [ 12 10 <repetition> score ] unit-test


